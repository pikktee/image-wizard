<?php

namespace Pikktee\ImageWizard;

use Illuminate\Database\Eloquent\Model;
use App;
use File;

class ImageHtml
{
    static public function img(
        $category, Model $model, $sizeId = 'default', $extension = 'jpg')
    {
        $path = '/storage/images/' . $category . '/';
        $basename = $model->slug . ($sizeId !== 'default' ? '-' . $sizeId : '');

        $imageSizes = config('imagewizard.imagesizes');
        $imageSize = $imageSizes[$sizeId];

        return
            '<img src="' . $path . $basename . '.' . $extension . '" ' .
                 'alt="' . $model->title . '"' .
                 'title="' . $model->title . '"' .
                 'srcset="' . $path . $basename . '-x2.' . $extension . ' 2x" ' .
                 'width="' . $imageSize['width'] . '" ' .
                 'height="' . $imageSize['height'] . '" ' .
            '/>';
    }
}
