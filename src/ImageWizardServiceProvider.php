<?php

namespace Pikktee\ImageWizard;

use Illuminate\Support\ServiceProvider;

class ImageWizardServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $this->publishes([
            __DIR__.'/config/imagewizard.php' => config_path('imagewizard.php'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Pikktee\ImageWizard\ImageWizardController');
        $this->mergeConfigFrom(
            __DIR__.'/config/imagewizard.php', 'imagewizard'
        );
    }
}
