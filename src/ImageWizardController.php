<?php

namespace Pikktee\ImageWizard;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Pikktee\ImageWizard\ImageResponseFactory;

class ImageWizardController extends Controller
{
    public function show($categorySlug, $imageSlug) {

        $response = ImageResponseFactory::make($categorySlug, $imageSlug);
        return $response->get();
    }
}
