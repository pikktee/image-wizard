<?php

namespace Pikktee\ImageWizard;

use App;
use File;

use Pikktee\ImageWizard\ImageInfo;

class ImageCache
{
    protected $cachePath = 'storage/app/public/images/';

    /**
     * Image info
     *
     * @var ImageInfo
     */
    protected $imageInfo;

    public function __construct(ImageInfo $imageInfo)
    {
        $this->imageInfo = $imageInfo;
    }

    public function exists()
    {
        return File::exists($this->filePath());
    }

    public function response()
    {
        return response()->file(
            $this->filePath(),
            ['Content-Type', 'image/' . $this->imageInfo->extension()]);
    }

    public function fileFolder()
    {
        return
            base_path($this->cachePath) .
            $this->imageInfo->category() . '/';
    }

    public function filePath()
    {
        return
            $this->fileFolder() .
            $this->imageInfo->slug();
    }
}
