<?php

namespace Pikktee\ImageWizard;

use Pikktee\ImageWizard\ImageInfo;
use Pikktee\ImageWizard\ImageResponse;
use Pikktee\ImageWizard\ImageCache;

class ImageResponseFactory
{
    private function __construct() {}

    static public function make($categorySlug, $imageSlug)
    {
        $imageInfo = self::makeImageInfo($categorySlug, $imageSlug);
        $imageCache = new ImageCache($imageInfo);

        return new ImageResponse($imageCache, $imageInfo);
    }

    static protected function makeImageInfo($categorySlug, $imageSlug)
    {
        try {
            $imageInfoClassName =
                'App\ImageWizard\\' . ucfirst($categorySlug) . 'Image';

            return new $imageInfoClassName($categorySlug, $imageSlug);
        } catch (\ReflectionException $e) {
            abort(404);
        }
    }
}
