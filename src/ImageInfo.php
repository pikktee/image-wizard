<?php

namespace Pikktee\ImageWizard;

use App;
use File;

abstract class ImageInfo
{
    protected $allowedImageSizes = ['default'];
    protected $defaultImage = 'assets/images/default.jpg';

    private $categorySlug;
    private $imageSlug;

    public function __construct($categorySlug, $imageSlug)
    {
        $this->categorySlug = $categorySlug;
        $this->imageSlug = $this->imageSlugParts($imageSlug);
    }

    public function filepath()
    {
        $filepath =
            $this->imageFolder($this->categorySlug) .
            $this->imageBasename($this->imageSlug['basename']) . '.' .
            $this->imageSlug['extension'];

        if (File::exists($filepath)) {
            return $filepath;
        }

        if ($this->defaultImage) {
            return resource_path($this->defaultImage);
        }

        return false;
    }

    public function width()
    {
        $imageSizes = config('imagewizard.imagesizes');
        return $imageSizes[$this->imageSlug['size']]['width'];
    }

    public function height()
    {
        $imageSizes = config('imagewizard.imagesizes');
        return $imageSizes[$this->imageSlug['size']]['height'];
    }

    public function extension()
    {
        return $this->imageSlug['extension'];
    }

    public function basename()
    {
        return $this->imageSlug['basename'];
    }

    public function isRetina()
    {
        return $this->imageSlug['retina'];
    }

    public function category()
    {
        return $this->categorySlug;
    }

    public function slug()
    {
        return $this->imageSlug['slug'];
    }

    protected function imageSlugParts($imageSlug)
    {
        $parts = pathinfo($imageSlug);
        $basename = $parts['filename'];

        // Retina?
        $retina = false;
        if (substr($basename, -3) === '-x2') {
            $basename = substr($basename, 0, strlen($basename) - 3);
            $retina = true;
        }

        $extension = isset($parts['extension']) ? $parts['extension'] : '';
        $size      = ltrim(strrchr($basename, '-'), '-');

        if (in_array($size, $this->allowedImageSizes)) {
            $basename = str_replace('-' . $size, '', $basename);
        } else {
            $size = 'default';
        }

        return [
            'slug'      => $imageSlug,
            'basename'  => $basename,
            'extension' => $extension,
            'size'      => $size,
            'retina'    => $retina
        ];
    }

    protected function imageFolder($categorySlug)
    {
        return resource_path('assets/images/' . $categorySlug .'/');
    }

    protected function imageBasename($baseNameFromUrl)
    {
        $baseNameOnFilesystem = $baseNameFromUrl;

        return $baseNameOnFilesystem;
    }
}
