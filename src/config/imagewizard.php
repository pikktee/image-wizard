<?php

return [
    'imagesizes' => [
        'default' => [
            'width' => 400,
            'height' => 300
        ],
        'thumbnail' => [
            'width' => 150,
            'height' => 150
        ]
    ]
];
