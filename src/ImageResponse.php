<?php

namespace Pikktee\ImageWizard;

use App;
use File;
use Image;

use Pikktee\ImageWizard\ImageCache;
use Pikktee\ImageWizard\ImageInfo;

class ImageResponse
{
    /**
     * Image cache
     * @var Pikktee\ImageWizard\ImageCache
     */
    protected $imageCache;

    /**
     * Image
     * @var Pikktee\ImageWizard\ImageInfo
     */
    protected $imageInfo;

    public function __construct(ImageCache $imageCache, ImageInfo $imageInfo)
    {
        $this->imageCache = $imageCache;
        $this->imageInfo = $imageInfo;
    }

    public function get()
    {
        // Check cache
        if ($this->imageCache->exists()) {
            return $this->imageCache->response();
        }

        $image = $this->makeImage();
        $this->saveImage($image);

        return $image->response($this->imageInfo->extension());
    }

    protected function makeImage()
    {
        $retinaFactor = $this->imageInfo->isRetina() ? 2 : 1;

        $width  = $this->imageInfo->width() * $retinaFactor;
        $height = $this->imageInfo->height() * $retinaFactor;

        return
            Image::make($this->imageInfo->filepath())
                ->fit($width, $height);
    }

    protected function saveImage($image)
    {
        if (!File::exists($this->imageCache->fileFolder())) {
            File::makeDirectory($this->imageCache->fileFolder(), 0775, true);
        }

        $image->save($this->imageCache->filePath());
    }
}
