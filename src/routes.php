<?php

Route::get(
    '/storage/images/{categorySlug}/{ImageSlug}',
    'Pikktee\ImageWizard\ImageWizardController@show'
);
